
/*  
//question board settings
r1 = 20; // radius of edge
l1 = 10; // legth clip extends beyond radius
t1 = 1;  // edge thickness
t2 = 1;  // top/bottom thickness
e1 = 8;  // how much the clip extrudes onto the board
h1 = 6.5;  // height of board
*/


// scrabble board settings
r1 = 4; // radius of edge
l1 = 20; // legth clip extends beyond radius
t1 = 1;  // edge thickness
t2 = 1;  // top/bottom thickness
e1 = 3;  // how much the clip extrudes onto the board
h1 = 6.5;  // height of board


$fa = 1;
$fs = 0.5;

module corner() {
    difference() {
        union() {
            intersection() {
                translate([l1, l1, 0])
                cylinder(r=r1+t1, h = h1+2*t2);
                
                cube([r1+l1+t1, r1+l1+t1, h1+2*t2]);
            }
            cube([l1, r1+l1+t1, h1+2*t2]);
            cube([r1+l1+t1, l1, h1+2*t2]);
        }
        translate([l1, l1, 0])
        cylinder(r=r1-e1, h = h1+2*t2+1);
            
        cube([l1, r1+l1-e1, h1+2*t2+1]);
        cube([r1+l1-e1, l1, h1+2*t2+1]);    
        
        
        translate([l1, l1, t2])
        cylinder(r=r1, h = h1);
                
        translate([0, 0, t2])
        cube([l1, r1+l1, h1]);
        
        translate([0, 0, t2])
        cube([r1+l1, l1, h1]);    
        
        rotate([0, 0, 45])
        cube(sqrt(2)*(r1+l1), center=true);   
    }
}
//!corner();

module edge() {
    /* 
    // Scrabble board
    l1 = 40; // legth of clip
    t1 = 1;  // edge thickness
    t2 = 1;  // top/bottom thickness
    e1 = 4;  // how much the clip extrudes onto the board
    h1 = 6.5;  // height of board
    */

    l1 = 40;
    t1 = 1;
    t2 = 1;
    e1 = 5;
    h1 = 5.8;

    difference() {
        cube([l1, t1+e1, h1+2*t2]);
        
        translate([0, 0, t2])
        cube([l1, e1, h1]);
        
        translate([0, t1+e1, 0])
        rotate([0, 0, -135])
        cube(8*e1);
        
        translate([l1, t1+e1, 0])
        rotate([0, 0, -135])
        cube(8*e1);
    }
    
    
}
edge();


//module edge