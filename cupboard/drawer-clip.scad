w1 = 64;   // width of the box
l1 = 130;  // length of the box Top part
//l1 = 138;

h1 = 6;    // height of the clips
g1 = 0.65; // gap between clips
r1 = 1;    // raduis of clip end thing
t1 = 1;    // thickness of the clips
t2 = 1;    // thickness of the top

$fs = 0.2;




module cutout(l) {
    difference() {
        cube([r1*2+g1, l, h1]);
        
        translate([0, 0, h1-r1])
        rotate([-90, 0, 0])
        cylinder(r = r1, h = l+1);
        
        translate([r1*2+g1, 0, h1-r1])
        rotate([-90, 0, 0])
        cylinder(r = r1, h = l+1);
    }
}


module clip_test() {
    union() {
        cube([t1*2+r1*2+g1, 10, 1]);
        
        cube([t1, 10, h1]);
        
        translate([t1, 0, h1-r1])
        rotate([-90, 0, 0])
        cylinder(h = 10, r = r1);
        
        translate([t1+r1*2+g1, 0, 0])
        cube([t1, 10, h1]);
        
        translate([t1+r1*2+g1, 0, h1-r1])
        rotate([-90, 0, 0])
        cylinder(h = 10, r = r1);
    }
}

module hook() {
    r1 = 8;
    t = 3;
    difference() {
        union() {
            cylinder(r = r1+t, h = 2);
            translate([0, -r1-t, 0])
            cube([r1+t, 2*(r1+t), 2]);
        }
        difference() {
            cylinder(r = r1, h = 2);
            translate([0, -r1, 0])
            cube([r1+t, 2*(r1), 2]);
        }
    }
}

//!hook();

difference() {
    a = r1+g1/2+t1;
    union() {
        translate([-a, -a, 0])
        cube([w1+2*a, l1+2*a, h1+t2-0.1]);
        
        translate([-1-2.65/2, l1/2, 0])
        //hook();
        difference() {
                cylinder(d = 20, h = 1+2.65+1);
                cylinder(d = 13, h = 1+2.65+1);
            }
    }
    translate([a, a, -0.1])
    cube([w1-2*a, l1-2*a, h1+t2+1]);
    
    translate([-a/2, -1, t2])
    cutout(l1+2);
    
    translate([w1-a/2, -1, t2])
    cutout(l1+2);
    
    rotate([0, 0, -90])
    translate([-a/2, -1, t2])
    cutout(w1+2);
    
    rotate([0, 0, -90])
    translate([-l1-a/2, -1, t2])
    cutout(w1+2);
}

module seperator_clip() {
    //rotate([0, -90, 0])
    difference() {
        //translate()
        union() {
            translate([-6, -2.65/2-1, 0])
            cube([10, 2.65+2, 7]);
            
            translate([-6, -2.65/2-1+l1, 0])
            cube([10, 2.65+2, 7]);
            
            translate([0, -2.65/2-1, 0])
            cube([1+2.65+1, l1+(2.65/2+1)*2, 7]);
            translate([0, l1/2, 0])
            rotate([0, 90, 0])
            difference() {
                cylinder(d = 20, h = 1+2.65+1);
                cylinder(d = 13, h = 1+2.65+1);
            }
        }
        translate([-10, 2.65/2, 1])
        rotate([-0, 0, -90])
        cutout(25);
        
        translate([0, l1, 0])
        translate([-10, 2.65/2, 1])
        rotate([-0, 0, -90])
        cutout(25);
        
        translate([-1, 0, 7])
        cube([1+2.65+3, l1, 7]);
        translate([1, 0, 1])
        cutout(l1);    
    }
    
    
}

module corner_clips() {
    difference() {
        cube([30, 30, 7]);
        
        translate([1+2.65+1, 1+2.65+1, -1])
        cube([40, 40, 9]);
        
        translate([1, 1, 1])
        cutout(l1);
        
        
        translate([1, 1+2.65, 1])
        rotate([0, 0, -90])
        cutout(l1);
    }
}

module top_clip() {
    //rotate([0, -90, 0])
    difference() {
        //translate()
        union() {
            translate([-26, -2.65/2-1, 0])
            cube([30, 2.65+2, 7]);
            
            translate([-26, -2.65/2-1+l1, 0])
            cube([30, 2.65+2, 7]);
            
            translate([0, -2.65/2-1, 0])
            cube([1+2.65+1, l1+(2.65/2+1)*2, 7]);
            translate([0, l1/2, 0])
            rotate([0, 90, 0])
            difference() {
                cylinder(d = 20, h = 1+2.65+1);
                cylinder(d = 13, h = 1+2.65+1);
            }
        }
        
        translate([-10, 0, 0])
        cube([10, l1, 20]);
        
        translate([-35+2.65/2+1, 2.65/2, 1])
        rotate([-0, 0, -90])
        cutout(35);
        
        translate([0, l1, 0])
        translate([-35+2.65/2+1, 2.65/2, 1])
        rotate([-0, 0, -90])
        cutout(35);
        
        translate([-1, 0, 7])
        cube([1+2.65+3, l1, 7]);
        translate([1, 0, 1])
        cutout(l1);    
    }
}

//!top_clip();
//!corner_clips();
//!seperator_clip();

//translate([t1, 0, 10]) !cutout(10);

//clip_test();